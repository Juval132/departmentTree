import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as go from 'gojs';
import { HttpClientService } from '../http-client.service';

const $ = go.GraphObject.make;

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css']
})
export class DiagramComponent {

  public selectedNode = null;
  public diagram: go.Diagram;
  public simpleView: boolean;
  // should get this from server
  public model: go.TreeModel = new go.TreeModel([]);

  constructor(private httpClient: HttpClientService) {}

  changeView()
  {
    this.simpleView = !this.simpleView
    let node = this.diagram.nodes;
    var newCat: string;
    newCat = this.simpleView ? "simple" : "detailed";
    while (node.next())
    {
      this.model.setCategoryForNodeData(node.value.data, newCat)
    }

  }
  public ngAfterViewInit(): void {
    this.diagram = $(go.Diagram, 'myDiagramDiv',
      {
        layout:
          $(go.TreeLayout,
            {
              isOngoing: true,
              treeStyle: go.TreeLayout.StyleLastParents,
              arrangement: go.TreeLayout.ArrangementHorizontal,
              // properties for most of the tree:
              angle: 90,
              layerSpacing: 35,
              // properties for the "last parents":
              alternateAngle: 90,
              alternateLayerSpacing: 35,
              alternateAlignment: go.TreeLayout.AlignmentBus,
              alternateNodeSpacing: 20
            }),
        'undoManager.isEnabled': true
      }
    );


    // define the Node template
    var detailtemplate =
    //this.diagram.nodeTemplate =
      $(go.Node, 'Auto',
      {resizable: true},
        // for sorting, have the Node.text be the data.name
        new go.Binding('text', 'name'),
        // bind the Part.layerName to control the Node's layer depending on whether it isSelected
        new go.Binding('layerName', 'isSelected', function(sel) { return sel ? 'Foreground' : ''; }).ofObject(),
        // define the node's outer shape

        $(go.Shape, "RoundedRectangle",
          {
            name: 'SHAPE', fill: 'lightblue', stroke: null,
            // set the port properties:
            portId: '', fromLinkable: true, toLinkable: true, cursor: 'pointer'
          },
          new go.Binding('fill', '', function(node) {
            // modify the fill based on the tree depth level
            const levelColors = ['#AC193D', '#2672EC', '#8C0095', '#5133AB',
              '#008299', '#D24726', '#008A00', '#094AB2'];
            let color = node.findObject('SHAPE').fill;
            const dia: go.Diagram = node.diagram;
            if (dia && dia.layout.network) {
              dia.layout.network.vertexes.each(function(v: go.TreeVertex) {
                if (v.node && v.node.key === node.data.key) {
                  const level: number = v.level % (levelColors.length);
                  color = levelColors[level];
                }
              });
            }
            return color;
          }).ofObject()
        ),
        $(go.Panel, 'Horizontal',

        $(go.Panel, "Horizontal",
        {              defaultAlignment: go.Spot.Right
        },        
        $(go.TextBlock, { font: '12pt  Segoe UI,sans-serif', stroke: 'white' },  // the name
        {
          margin:10,
          column: 0, columnSpan: 5,
          editable: false, isMultiline: true,
          minSize: new go.Size(10, 16)
        },
        new go.Binding('text', 'name').makeTwoWay()),

        $(go.TextBlock, {font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the name
        {
          row: 0, column: 0, columnSpan: 5,
          font: '12pt Segoe UI,sans-serif',
          editable: false, isMultiline: false,
          minSize: new go.Size(10, 16)
        },
        new go.Binding('text', 'id').makeTwoWay()),


        $(go.TextBlock, '', { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
        { 
          row: 1, column: 0,
        })
        ,
      $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
        {
          row: 1, column: 1, columnSpan: 4,
          editable: false, isMultiline: false,
          minSize: new go.Size(10, 14)
        },

        new go.Binding('text', 'soldier').makeTwoWay()),
      $(go.TextBlock, { visible:false, font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
        { row: 2, column: 0 },
      new go.Binding('text', 'key')),
      $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the comments
        {
          
          row: 3, column: 0, columnSpan: 5,
          font: 'italic 16pt sans-serif',
          wrap: go.TextBlock.WrapFit,
          editable: false,  // by default newlines are allowed
          minSize: new go.Size(10, 14)
        },
        new go.Binding('text', 'rank').makeTwoWay())
        ),        
        $(go.Shape, "MinusLine", {angle:90, fill: "transparent", alignment: go.Spot.Left}), 


          // define the panel where the text will appear
          $(go.Panel, 'Table',

            {
              //minSize: new go.Size(300,900),
              maxSize: new go.Size(400, 999),
              margin: new go.Margin(6, 10, 0, 3),
              defaultAlignment: go.Spot.Right
            },
            $(go.RowColumnDefinition, { column: 2, width: 4 }),

            $(go.TextBlock, { font: '24pt  Segoe UI,sans-serif', stroke: 'white', alignment:go.Spot.Center},
              {
                row: 0, column: 5, columnSpan: 1,
                editable: false, isMultiline: false,
              },
              new go.Binding('text', 'title').makeTwoWay()),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
              { name: 'boss', row: 2, column: 3 }, // we include a name so we can access this TextBlock when deleting Nodes/Links
              new go.Binding('text', 'COname')),
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the comments
              {
                row: 3, column: 0, columnSpan: 5,
                font: 'italic 9pt sans-serif',
                wrap: go.TextBlock.WrapFit,
                editable: false,  // by default newlines are allowed
                minSize: new go.Size(10, 14)
              },

              new go.Binding('text', 'comments').makeTwoWay()),
              $("Button",
              { alignment: go.Spot.TopRight },
              $(go.Shape, "MinusLine", { width: 8, height: 8 }),
              { click: function(e,obj)
                {
                  var node = obj.part;
                  if (node) {
                    var diagram = node.diagram;
                    diagram.startTransaction("changeCategory");
                    var cat = diagram.model.getCategoryForNodeData(node.data);
                    if (cat === "simple")
                      cat = "detailed";
                    else
                      cat = "simple";
                    diagram.model.setCategoryForNodeData(node.data, cat);
                    diagram.commitTransaction("changeCategory");
                
                    
                }
                }
              }

            )  // end Table Panel
          ,
          
        ), // end Horizontal Panel
        
        
     $("TreeExpanderButton",
     { alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top },

        { click: function(e, node) { 
          var realNode = e.diagram.findNodeForData(node.part.data);
          if (realNode.isTreeExpanded)
            realNode.collapseTree();
            else
              realNode.expandTree()

            } },
        ),

      ));  // end Node

      var detailedOutTemplate =
      //this.diagram.nodeTemplate =
        $(go.Node, 'Auto', 
        {resizable: true},
          // for sorting, have the Node.text be the data.name
          new go.Binding('text', 'name'),
          // bind the Part.layerName to control the Node's layer depending on whether it isSelected
          new go.Binding('layerName', 'isSelected', function(sel) { return sel ? 'Foreground' : ''; }).ofObject(),
          // define the node's outer shape
  
          $(go.Shape, 'RoundedRectangle',
            {
              name: 'SHAPE', fill: 'lightblue', opacity:0.25 ,stroke: null,
              // set the port properties:
              portId: '', fromLinkable: true, toLinkable: true, cursor: 'pointer'
            },
            new go.Binding('fill', '', function(node) {
              // modify the fill based on the tree depth level
              const levelColors = ['#AC193D', '#2672EC', '#8C0095', '#5133AB',
                '#008299', '#D24726', '#008A00', '#094AB2'];
              let color = node.findObject('SHAPE').fill;
              const dia: go.Diagram = node.diagram;
              if (dia && dia.layout.network) {
                dia.layout.network.vertexes.each(function(v: go.TreeVertex) {
                  if (v.node && v.node.key === node.data.key) {
                    const level: number = v.level % (levelColors.length);
                    color = levelColors[level];
                  }
                });
              }
              return color;
            }).ofObject()
          ),
          $(go.Panel, 'Horizontal',
  
          $(go.Panel, "Horizontal",
          {              defaultAlignment: go.Spot.Right
          },        
          $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the name
          {
            row: 0, column: 0, columnSpan: 5,
            font: '12pt Segoe UI,sans-serif',
            editable: false, isMultiline: false,
            minSize: new go.Size(10, 16)
          },
          new go.Binding('text', 'name').makeTwoWay()),
  
          $(go.TextBlock, {font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the name
          {
            row: 0, column: 0, columnSpan: 5,
            font: '12pt Segoe UI,sans-serif',
            editable: false, isMultiline: false,
            minSize: new go.Size(10, 16)
          },
          new go.Binding('text', 'id' ).makeTwoWay()),
  
          $(go.TextBlock, { visible:false, font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
          { 
            row: 1, column: 0,
          })
          ,
        $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
          {
            row: 1, column: 1, columnSpan: 4,
            editable: false, isMultiline: false,
            minSize: new go.Size(10, 14)
          },
  
  
          
          new go.Binding('text', 'soldier').makeTwoWay()),
        $(go.TextBlock, '' , { visible:false, font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
          { row: 2, column: 0 },
          new go.Binding('text', 'key')),
        $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the comments
          {
            
            row: 3, column: 0, columnSpan: 5,
            font: 'italic 16pt sans-serif',
            wrap: go.TextBlock.WrapFit,
            editable: false,  // by default newlines are allowed
            minSize: new go.Size(10, 14)
          },
          new go.Binding('text', 'rank').makeTwoWay())
          ),
  
          $(go.Panel, "Spot",
          new go.Binding("opacity", "ribbon", function(t) { return t ? 1 : 0; }),
          // note that the opacity defaults to zero (not visible),
          // in case there is no "ribbon" property
          { opacity: 0,
            alignment: new go.Spot(1, 0, 5, -5),
            alignmentFocus: go.Spot.TopRight },
          $(go.Shape,  // the ribbon itself
            { geometryString: "F1 M0 0 L30 0 70 40 70 70z",
              fill: "red", stroke: null, strokeWidth: 0 }),
          $(go.TextBlock,
            new go.Binding("text", "ribbon"),
            { alignment: new go.Spot(1, 0, -29, 29),
              angle: 45, maxSize: new go.Size(100, NaN),
              stroke: "white", font: "bold 13px sans-serif", textAlign: "center" })
        ),  
          $(go.Shape, "MinusLine", {angle:90, fill: "transparent", alignment: go.Spot.Left}), 
  
  
            // define the panel where the text will appear
            $(go.Panel, 'Table',
  
              {
                //minSize: new go.Size(300,900),
                maxSize: new go.Size(400, 999),
                margin: new go.Margin(6, 10, 0, 3),
                defaultAlignment: go.Spot.Right
              },
              $(go.RowColumnDefinition, { column: 2, width: 4 }),
  
              $(go.TextBlock, { font: '24pt  Segoe UI,sans-serif', stroke: 'white', alignment:go.Spot.Center},
                {
                  row: 0, column: 5, columnSpan: 1,
                  editable: false, isMultiline: false,
                },
                new go.Binding('text', 'title').makeTwoWay()),
              $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },
                { name: 'boss', row: 2, column: 3 }, // we include a name so we can access this TextBlock when deleting Nodes/Links
                new go.Binding('text', 'COname')),
              $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the comments
                {
                  row: 3, column: 0, columnSpan: 5,
                  font: 'italic 9pt sans-serif',
                  wrap: go.TextBlock.WrapFit,
                  editable: false,  // by default newlines are allowed
                  minSize: new go.Size(10, 14)
                },
  
                new go.Binding('text', 'comments').makeTwoWay()),
                $("Button",
                { alignment: go.Spot.TopRight },
                $(go.Shape, "MinusLine", { width: 8, height: 8 }),
                { click: function(e,obj)
                  {
                    var node = obj.part;
                    if (node) {
                      var diagram = node.diagram;
                      diagram.startTransaction("changeCategory");
                      var cat = diagram.model.getCategoryForNodeData(node.data);
                      if (cat === "simple")
                        cat = "detailed";
                      else
                        cat = "simple";
                      diagram.model.setCategoryForNodeData(node.data, cat);
                      diagram.commitTransaction("changeCategory");
                  
                      
                  }
                  }
                }
  
              )  // end Table Panel
            ,
            
          ), // end Horizontal Panel
          
          
       $("TreeExpanderButton",
       { alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top },
  
          { click: function(e, node) { 
            var realNode = e.diagram.findNodeForData(node.part.data);
            if (realNode.isTreeExpanded)
              realNode.collapseTree();
              else
                realNode.expandTree()
  
              } },
          ),
  
        ));  // end Node
  






var simpletemplate =
    //this.diagram.nodeTemplate =
      $(go.Node, 'Auto',
      {resizable: true},

        // bind the Part.layerName to control the Node's layer depending on whether it isSelected
        new go.Binding('layerName', 'isSelected', function(sel) { return sel ? 'Foreground' : ''; }).ofObject(),
        // define the node's outer shape
      
        $(go.Shape, 'RoundedRectangle',
          {
            name: 'SHAPE', fill: 'lightblue', stroke: null,
            // set the port properties:
            portId: '', fromLinkable: true, toLinkable: true, cursor: 'pointer'
          },
          new go.Binding('fill', '', function(node) {
            // modify the fill based on the tree depth level
            const levelColors = ['#AC193D', '#2672EC', '#8C0095', '#5133AB',
              '#008299', '#D24726', '#008A00', '#094AB2'];
            let color = node.findObject('SHAPE').fill;
            const dia: go.Diagram = node.diagram;
            if (dia && dia.layout.network) {
              dia.layout.network.vertexes.each(function(v: go.TreeVertex) {
                if (v.node && v.node.key === node.data.key) {
                  const level: number = v.level % (levelColors.length);
                  color = levelColors[level];
                }
              });
            }
            return color;
          }).ofObject()
        ),

          // define the panel where the text will appear
          $(go.Panel, 'Table',

            {
              //minSize: new go.Size(300,900),
              maxSize: new go.Size(400, 999),
              margin: new go.Margin(6, 10, 0, 3),
              defaultAlignment: go.Spot.Right
            },
            $(go.RowColumnDefinition, { column: 2, width: 4 }),

            $(go.TextBlock, { font: '24pt  Segoe UI,sans-serif', stroke: 'white', alignment:go.Spot.Center},
              {
                row: 0, column: 5, columnSpan: 1,
                editable: false, isMultiline: false,
              },
              new go.Binding('text', 'title').makeTwoWay()),

              $(go.TextBlock, {visible:false, font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the name
              {
                row: 0, column: 0, columnSpan: 5,
                font: '12pt Segoe UI,sans-serif',
                editable: false, isMultiline: false,
                minSize: new go.Size(10, 16)
              },
              new go.Binding('text', 'id' ).makeTwoWay()),
      
      
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },),
    
            $(go.TextBlock, { font: '9pt  Segoe UI,sans-serif', stroke: 'white' },  // the comments
              {
                row: 3, column: 0, columnSpan: 5,
                font: 'italic 9pt sans-serif',
                wrap: go.TextBlock.WrapFit,
                editable: false,  // by default newlines are allowed
                minSize: new go.Size(10, 14)
              },

              new go.Binding('text', 'comments').makeTwoWay()),
              $("Button",
              { alignment: go.Spot.TopRight },
              $(go.Shape, "MinusLine", { width: 8, height: 8 }),
              { click: function(e,obj)
                {
                  var node = obj.part;
                  if (node) {
                    var diagram = node.diagram;
                    var cat = diagram.model.getCategoryForNodeData(node.data);
                    if (cat === "simple")
                      cat = "detailed";
                    else
                      cat = "simple";
                    diagram.model.setCategoryForNodeData(node.data, cat);
                                    
                    
                }
                }
              }

            )  // end Table Panel
          ,
          
        ), // end Horizontal Panel
          
      
     $("TreeExpanderButton",
     { alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top },

        { click: function(e, node) { 
          var realNode = e.diagram.findNodeForData(node.part.data);
          if (realNode.isTreeExpanded)
            realNode.collapseTree();
            else
              realNode.expandTree()

            } },
        ),

      );  // end Node



      var templmap = new go.Map<string, go.Node>(); // In TypeScript you could write: new go.Map<string, go.Node>();
      templmap.add("simple", simpletemplate);
      templmap.add("detailed", detailtemplate);
      templmap.add("filtered", detailedOutTemplate)
      this.diagram.nodeTemplateMap = templmap
      
      //this.diagram.layout = $(go.TreeLayout);
      this.diagram.nodeTemplate = detailtemplate;
      this.simpleView = false;
      this.diagram.model = this.model;
      let node = this.diagram.nodes;

    // when the selection changes, emit event to app-component updating the selected node
    this.diagram.addDiagramListener('ChangedSelection', () => {
      this.selectedNode = this.diagram.selection.first();
    });
  }
}



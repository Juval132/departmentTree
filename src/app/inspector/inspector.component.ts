import { UpperCasePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import * as go from 'gojs';
import { DiagramComponent } from '../diagram/diagram.component';
import { FilterPipe } from '../filter.pipe';
import { HttpClientService } from '../http-client.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ValueTransformer } from '@angular/compiler/src/util';
import { jsonpCallbackContext } from '@angular/common/http/src/module';
import { computeStyle } from '@angular/animations/browser/src/util';




@Component({
  selector: 'app-inspector',
  templateUrl: './inspector.component.html',
  styleUrls: ['./inspector.component.css']
})

export class InspectorComponent {

  public divisionSoldiers: any[];
  public baseSoldiers: any[];
  public allDivisions : string[];
  public ranksToDisplay: string[];
  Tree : string;

  @Input()
  public diagram: go.Diagram;

  @Input()
  public model: go.Model;
  ranksFilter: FormGroup;
  public _selectedNode: go.Node;
  public nonCommanders : any[];
  public shouldAdd = false;
  public showUpdate = false;
  public showFilters = false;
  public formerSoldier = "";
  public displaySettings = false;
  public expanded = false;
  public isFiltered = true;
  public filterMsg: string;
  public filterDate;
  public data = {
    id: null,
    name: null,
    rank: null,
    parent: null,
    title: null,
    roleId: null,
    roleCommanderId: null,
  };
  public show = {
    soldiers: false,
    commanders: false,
    chosenSoldier: "",
    chosenCommander: ""
  }

  form: FormGroup;
  Data: Array<any> = [
    { name: 'חיילי חובה', value: 'conscripts' },
    { name: 'קבע ראשוני', value: 'primal' },
    { name: 'קבע מובהק', value: 'permanent' },
    { name: 'קצינים', value: 'officers' },
    { name: 'נגדים', value: 'NCO' }
  ];

  @Input()
  get selectedNode() { return this._selectedNode; }

  set selectedNode(node: go.Node) {
    this.show.soldiers = false
    this.show.commanders = false
    this.shouldAdd = false;
    if (!this.nonCommanders)
    {
      this.nonCommanders = []
    }
    if (!this.divisionSoldiers)
    {
      this.divisionSoldiers = []
    }
    if (node)
    {
      if (this._selectedNode) // if selected other soldier before.
      {
        this.nonCommanders.forEach(e => {
          this.divisionSoldiers.push(e)
        });
      }
      this._selectedNode = node;
      this.data.id = this._selectedNode.data.id;
      this.data.name = this._selectedNode.data.name;
      this.data.rank = this._selectedNode.data.rank;
      this.data.roleId = this._selectedNode.data.roleId;
      
      let parent = this.model.findNodeDataForKey(this._selectedNode.data.parent)
      if (parent)
      {
        this.data.parent = parent.name;
      }
      this.data.title = this._selectedNode.data.title;
      let nonCommandersRoleIds = this.getSonsRoleIds(this.selectedNode)
      this.nonCommanders = this.divisionSoldiers.filter(e => nonCommandersRoleIds.includes(e.roleId))
      this.divisionSoldiers = this.divisionSoldiers.filter(e => !this.nonCommanders.includes(e))
    } else 
      {
        this.nonCommanders.forEach(e => {
          this.divisionSoldiers.push(e)
        });
        this._selectedNode = null;
        this.data.id = null;
        this.data.name = null
        this.data.rank = null
        this.data.parent = null
        this.data.roleId = null
        this.data.title = null
        this.data.roleCommanderId = null
      }
  }
  constructor(private httpClient: HttpClientService, fb: FormBuilder) {
    this.form = fb.group({
      ranksFiltersArr: fb.array([], [Validators.required])
    })

  }

  onCheckboxChange(e) {
    const ranksFiltersArr: FormArray = this.form.get('ranksFiltersArr') as FormArray;

    if (e.target.checked) {
      ranksFiltersArr.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      ranksFiltersArr.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          ranksFiltersArr.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
      
  ngOnInit()
  {
    this.show.soldiers = false
    this.baseSoldiers = []
    this.httpClient.getAllTrees().subscribe((data) => {
      this.allDivisions = data
      this.Tree = this.allDivisions[0]
      this.displayTree(this.Tree)
    })
    this.httpClient.getAllSoldiers().subscribe((data) =>
    {
      this.baseSoldiers = data.filter(soldier => soldier.division == '');
    })    
  }

  public commanderChoice(commander:any)
  {
    if (commander.name)
    {
      this.data.parent = commander.name //+ " " + commander.role + "  #" + commander.roleId
      this.show.chosenCommander = commander.name
    }
    else
    {
      this.data.parent = commander.role //+ " " + commander.role + "  #" + commander.roleId
      this.show.chosenCommander = commander.role

    }
    this.data.roleCommanderId = commander.roleId 
  }

  public commanderSearchTable()
  {
    if (this.show.chosenCommander == this.data.parent) // if soldier was selected before, or name is ""
    {
        this.show.commanders = false
    }
    else
    {
      this.data.roleCommanderId = null
      this.show.commanders = true
    }

    if (this.data.parent =="")
    {
      this.show.commanders = false
    }

  }


  public commanderFocus()
  {
      this.show.commanders = !this.show.commanders;
      if (this.data.parent == this.show.chosenCommander)
      {
        this.show.commanders = false
      }
  
  }


  public soldierChoice(soldier:any)
  {
    if (this.showUpdate)
    {
      this.formerSoldier = this.data.id
    }
    this.data.id = soldier.id
    this.data.name = soldier.name
    this.show.chosenSoldier = soldier.name
    this.data.rank = soldier.rank
  }
  public soldierSearchTable()
  {
    if (this.show.chosenSoldier == this.data.name) // if soldier was selected before, or name is ""
    {
        this.show.soldiers = false
    }
    else 
    {
      this.data.id == null
      this.show.soldiers = true
    }
    if (this.data.name =="")
    {
      this.show.soldiers = false
    }
  }

  
  public soldierFocus()
  {
    this.show.soldiers = !this.show.soldiers;
    if (this.data.name == this.show.chosenSoldier)
    {
      this.show.soldiers = false
    }
  }
  
  public onCommitForm() 
  {
      this.showUpdate = false;
      if (!this.data.roleCommanderId)
      {
        this.data.roleCommanderId = this.selectedNode.data.parent
      }
      this.model.startTransaction();
      this.model.set(this.selectedNode.data, 'name', this.data.name);
      this.model.set(this.selectedNode.data, 'parent', this.data.roleCommanderId);
      this.model.set(this.selectedNode.data, 'title', this.data.title);
      let updatedSoldier =
      {
        id: this.data.id,
        role: this.data.title,
        roleId: this.selectedNode.key,
        roleCommanderId: this.data.roleCommanderId
      }

      if (updatedSoldier.id != this.formerSoldier && this.formerSoldier)
      {
        this.baseSoldiers = this.baseSoldiers.filter(x => x.id != this.data.id)// should remove new soldier from base soldier.
        let soldier =  this.divisionSoldiers.find(x => x.id == this.formerSoldier)
        if (soldier)
        {
          delete soldier.role
          delete soldier.roleId
          delete soldier.release
          this.baseSoldiers.push(soldier)// should push former soldier to base soldiers.
        }
      }
      this.model.commitTransaction();
      this.httpClient.ChagneSoldierData(updatedSoldier).subscribe(() => 
      {
        this.displayTree(this.Tree) 
      });

    }

    public cleanTree()
    {
      while(this.model.nodeDataArray.length>0)
      {
        this.model.removeNodeData(this.model.nodeDataArray[0])
      }
          
    }
    public deleteTree()
    {
      if (!confirm("האם למחוק את העץ הנוכחי? "))
      {
        return;
      }
      this.httpClient.DeleteTree(this.Tree).subscribe((data) => 
      {
         let soldiersDeletedList = JSON.parse(data.body)
        soldiersDeletedList.forEach(element => {
          delete element["release"]
          this.baseSoldiers.push(element)
        }) 
      });
      this.allDivisions = this.allDivisions.filter(e => e != this.Tree)
      console.log(this.allDivisions)
      this.displayTree(this.allDivisions[this.allDivisions.length-1])
    }

    public addTree()
    {
      let nd = prompt("הכנס את שם המחלקה החדשה:", "")
      if (nd =="" || nd == null)
        return;
      if (this.allDivisions.includes(nd))
      {
        alert("יש מחלקה קיימת העונה לשם שבחרת.")
        return;
      }
      this.httpClient.AddTree(nd)
      this.allDivisions.push(nd)
      this.displayTree(nd)
    }

    public getCurrentDate() : Date
    {
      return new Date()
    }
    public changeDateFormat(date : string) : string
    {
      var helper = date.split('-')
      date = helper[2]+ "." + helper[1] +"." + helper[0]
      return date
    }
    public displayTree(tree = this.Tree, displayDate = this.getCurrentDate() , ranks = ["conscripts", "primal", "permanent", "officers", "NCO"])
    {
      
      this.selectedNode = null;
      this.data.id = null;
      this.data.name = null
      this.data.parent = null
      this.data.rank = null
      this.data.roleId = null
      this.data.roleCommanderId = null
      this.data.title = null
      this.cleanTree();

      this.Tree = tree;
      this.divisionSoldiers = []
      this.httpClient.getTreeNodes(tree).subscribe((data) => {
        data.forEach(node => {
          let soldier =
          {
            name: node["name"],
            id: node["id"],
            rank: node["rank"],
            role: node["role"],
            roleId: node["roleId"],
            release: node["release"] +""
          }
            this.divisionSoldiers.push(soldier)
            this.model.addNodeData({'key': soldier.roleId, 'id':soldier.id, 'name': soldier.name, 'rank': soldier.rank, 'title': node["role"], figure: "RoundedRectangle",});
            let nodeData = this.model.findNodeDataForKey(soldier.roleId)
            if (node["roleCommanderId"])
            {
              this.model.setDataProperty(nodeData, "parent", node["roleCommanderId"])
            }
            if (this.showFilters)
            {
              ranks = this.form.value["ranksFiltersArr"]
              if (!this.getRankStatus(soldier.rank).some(e=> ranks.includes(e)))
              {
                this.model.setDataProperty(nodeData, "category", "filtered")
                console.log(soldier.name)
                if (soldier.name=="")
                {
                  this.model.setDataProperty(nodeData, "ribbon", "לא ידוע")
                }
                else
                {
                  this.model.setDataProperty(nodeData, "ribbon", "לא עומד בדרגה")
                }
              }
              if (displayDate!= this.getCurrentDate() && this.filterDate && soldier.release)
              {
                if (soldier.name=="")
                {
                  this.model.setDataProperty(nodeData, "ribbon", "לא ידוע")
                }
                else if (new Date(soldier.release) < new Date(this.changeDateFormat(this.filterDate)))
                {
                  this.model.setDataProperty(nodeData, "category", "filtered")
                  this.model.setDataProperty(nodeData, "ribbon", "השתחרר")
                }
              }

            }

        });
        if(this.showFilters)
        {
          this.showFilters = false;
          this.displaySettings = false;
          const arr = <FormArray>this.form.controls.ranksFiltersArr;
          arr.controls = [];
          this.filterMsg = "המחלקה סוננה להצגת הקריטריונים הבאים:"
          this.filterMsg +=" "

          let ranks = this.form.value["ranksFiltersArr"]
          if (ranks)
          { //"conscripts", "primal", "permanent", "officers", "NCO"
            console.log(ranks)
            if (ranks.includes("conscripts"))
            {
              this.filterMsg +="חיילי חובה"
              this.filterMsg +=", "
            }
            if (ranks.includes("primal"))
            {
              this.filterMsg +="קבע ראשוני"
              this.filterMsg +=", "
            }            
            if (ranks.includes("permanent"))
            {
              this.filterMsg +="קבע מובהק"
              this.filterMsg +=", "
            }
            if (ranks.includes("officers"))
            {
              this.filterMsg +="קצינים"
              this.filterMsg +=", "
            }
            if (ranks.includes("NCO"))
            {
              this.filterMsg +="נגדים"
              this.filterMsg +=", "
            }
            if (this.filterDate)
            {
              this.filterMsg +="החל מהתאריך: " +this.changeDateFormat(this.filterDate)
              this.filterMsg +=", "
            }
          }
        }
        else
        {
          this.filterMsg = null;
        }
      });
    }



    public onDelete()
    {
      let isInclude = false;
      let roleId = this.model.getKeyForNodeData(this._selectedNode);
      let id = this._selectedNode.data.id
      if(this.selectedNode.findTreeChildrenLinks().count > 0)
      {
        if(confirm("למחוק גם את הפקודים?")) 
          {
            isInclude = true;
          }
      }
      
      this.httpClient.DeleteNode(this.Tree, roleId.toString(), isInclude).subscribe((data) => 
      {
         let soldiersDeletedList = JSON.parse(data.body)
        soldiersDeletedList.forEach(element => {
          delete element["release"]
          this.baseSoldiers.push(element)
        })
        this.cleanTree()
        this.displayTree(this.Tree)
  
      });
    }

  public onAddForm() {
    this.shouldAdd = false
    this.model.startTransaction();
    if (this.data.roleCommanderId == null)
    {
      this.data.roleCommanderId = "null"
    }
    let soldier = {
      id: this.data.id,
      role: this.data.title,
      roleCommanderId: this.data.roleCommanderId
    }
    this.httpClient.AddNode(this.Tree, soldier).subscribe((data) => 
    {
      data = JSON.parse(data.body)
      this.data.roleId = data["roleId"]
      this.model.addNodeData({ 'id' : this.data.id, 'key': this.data.roleId, 'name': this.data.name, 'rank': this.data.rank, 'title': this.data.title, 'parent' : this.data.roleCommanderId, 'category': this.model.getCategoryForNodeData(this.model.findNodeDataForKey(this.data.roleCommanderId))});
      
      let soldier =
      {
        name: this.data.name,
        id: this.data.id,
        rank: this.data.rank,
        role: this.data.title,
        roleId: this.data.roleId,
        release: data["release"]
      }
    
      this.divisionSoldiers.push(soldier)
      this.baseSoldiers = this.baseSoldiers.filter(e => e.id != soldier.id)
      this._selectedNode = null;
      this.data.id = null;
      this.data.name = null
      this.data.rank = null
      this.data.parent = null
      this.data.roleId = null
      this.data.title = null
      this.data.roleCommanderId = null

    })
    //get RoleId from server.
    
  }
  public getRankStatus(rank: string)
  {
    let index;
    let ranks = []
    const cons = ['טוראי', 'רב"ט', 'סמל', 'סמ"ר'];
    const primal = ['סמ"ר', 'רס"ל', 'רס"ר', 'סג"ם', 'סגן'];
    const permanent = ['סרן', 'רס"ן', 'סא"ל', 'אל"מ', 'רס"ם', 'רס"ב','רנ"ג'];
    const officers = ['סג"ם', 'סגן', 'סרן', 'רס"ן', 'סא"ל', 'אל"מ'];
    const NCO = ['סמ"ר', 'רס"ל', 'רס"ר', 'רס"ם', 'רס"ב', 'רנ"ג'];
    const allRanks = [cons, primal, permanent, officers, NCO]
    const ranksStat = ["conscripts", "primal", "permanent", "officers", "NCO"]
    for (index = 0; index < allRanks.length; index++) 
    {
      if (allRanks[index].includes(rank))
      {
        ranks.push(ranksStat[index])
      }
    }  
    return ranks
  }

  public getSonsRoleIds(parent :go.Node ) : Array<number>
  {
    let sons: number[] = []
    if (!parent)
    {
      return []
    }
    let children = parent.findTreeChildrenNodes()
    children.each(node=> {
      this.getSonsRoleIds(node).forEach(e => {
        sons.push(e)
      });
    })
    sons.push(parseInt(parent.key.toString())) 
    return sons
  }

}

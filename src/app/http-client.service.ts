import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Panel } from 'gojs';
import { InspectorComponent } from './inspector/inspector.component';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
   headers = {'content-type' : 'application/json'}

  url = "http://localhost:8080/";
  constructor(private http: HttpClient) { }

  
getAllSoldiers() : Observable<any>
{
  return this.http.get<any>(this.url+'AllSoldiers')
}


  getAllTrees() : Observable<any>
  {
    return this.http.get<any>(this.url+'AllTrees')
  }

  getTreeNodes(Tree : string) : Observable<any>
  {   
     return this.http.get<any>(this.url + Tree + "/nodes")
  }

  AddNode(Tree : string, soldier : any) : Observable<any>
  {
    const body = soldier
    let answer = this.http.post(this.url + "AddNode/" + Tree ,body, {'headers'  :this.headers, observe: 'response', responseType:'text'})
    return answer
    
  }

  AddTree(Tree : string)
  {
    const body = '{}'
    this.http.post(this.url + "AddTree/" + Tree,body, {'headers'  :this.headers, observe: 'response', responseType:'text'}).subscribe(() => {})
  }

  ChagneSoldierData(newData : any) : Observable<any> 
  {
    const body = newData
    return this.http.put(this.url+"updateSoldierData",body, {'headers'  :this.headers, observe: 'response', responseType:'text'})
  }

  DeleteNode(Tree: string, RoleId : string, includeSub : boolean) : Observable<any>
  {
    return this.http.delete(this.url + Tree+'/Roleid=' + RoleId + "/includeSub=" + includeSub, {'headers'  :this.headers, observe: 'response', responseType:'text'})
  }
  DeleteTree(tree : string): Observable<any>
  {
    return this.http.delete(this.url + "DeleteTree/" + tree, {'headers'  :this.headers, observe: 'response', responseType:'text'})
  }


}

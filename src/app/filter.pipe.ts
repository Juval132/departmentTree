import { Pipe, PipeTransform } from '@angular/core';
import { isBoolean } from 'util';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] 
  {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return [];
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      let idRE = false, nameRE = false, roleRE = false
      if (it.role != null)
      {
        roleRE = it.role.includes(searchText)
      }
      if (it.id != null)
      {
        idRE = it.id.startsWith(searchText)
      }
      if (it.name != null)
      {
        nameRE = it.name.includes(searchText)
      }

      return (nameRE || roleRE || idRE);
    });
  }
}
